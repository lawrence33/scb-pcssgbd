package br.org.unirio.controller;

import java.util.List;

import br.org.unirio.model.PedidoAposentaBicicleta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import br.org.unirio.exception.BicicletaNotFoundException;
import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.Bicicleta;
import br.org.unirio.model.PedidoConsertoBicicleta;
import br.org.unirio.service.BicicletaService;
import io.javalin.http.Context;

@RestController
public class BicicletaController {

	@Autowired
	private BicicletaService bs;

	private final String PATH_BICICLETA = "/bicicleta";

	@PostMapping(PATH_BICICLETA + "/conserto")
	public void consertaBicicleta(@RequestParam Integer idFuncionario, @RequestParam Integer idBicicleta) {
		try {
			bs.criaPedidoConsertoBicicleta(idBicicleta, idFuncionario);
		} catch (ObjetoJaExisteException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(PATH_BICICLETA + "/conserto")
	public List<PedidoConsertoBicicleta> getConsertosBicicleta() {
		return bs.getPedidosDeConserto();
	}

	@PostMapping(PATH_BICICLETA + "/aposenta")
	public void aposentaBicicleta(@RequestParam Integer idFuncionario, @RequestParam Integer idBicicleta) {
		try {
			bs.criaPedidoAposentaBicicleta(idBicicleta, idFuncionario);
		} catch (ObjetoJaExisteException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(PATH_BICICLETA + "/aposenta")
	public List<PedidoAposentaBicicleta> getAposentadas() {
		return bs.getPedidosDeAposentadoria();
	}

	@PostMapping(PATH_BICICLETA)
	public void criaBicicleta(@RequestBody Bicicleta bicicleta) {
		try {
			bs.criaBicicleta(bicicleta);
		} catch (ObjetoJaExisteException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(PATH_BICICLETA)
	public Bicicleta getBicicleta(@RequestParam int idBicicleta) {
		try {
			return bs.getBicicleta(idBicicleta);
		} catch (BicicletaNotFoundException bnfe) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping(PATH_BICICLETA)
	public void deleteBicicleta(@RequestParam int idBicicleta) {

		try {
			bs.deletaBicicleta(idBicicleta);
		} catch (BicicletaNotFoundException bnfe) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
}
