package br.org.unirio.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import br.org.unirio.exception.BicicletaIndisponivelException;
import br.org.unirio.exception.CiclistaNotFoundException;
import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.Ciclista;
import br.org.unirio.service.CiclistaService;
import io.javalin.http.Context;

@RestController
public class CiclistaController {
    @Autowired
    CiclistaService cs;

	private final String PATH_CICLISTA = "/ciclista";


	@GetMapping(PATH_CICLISTA)
    public List<Ciclista> getCiclista(@RequestParam(required = false) Integer idCiclista, @RequestParam(required = false) String nome, @RequestParam(required = false) String email) {
        try {
            if(idCiclista == null)
                idCiclista = 0;
            return cs.getCiclista(idCiclista, nome, email);
        } catch (CiclistaNotFoundException cnfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(PATH_CICLISTA)
    public void criaCiclista(@RequestBody Ciclista ciclista) {
        try {
            cs.criaCiclista(ciclista);
        } catch (ObjetoJaExisteException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(PATH_CICLISTA)
    public void deletaCiclista(@RequestParam Integer idCiclista) {

        try {
            cs.deletaCiclista(idCiclista);
        } catch (CiclistaNotFoundException fnfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(PATH_CICLISTA + "/alugaBicicleta")
    public void alugaBicicleta(@RequestParam int idBicicleta, @RequestParam int idCiclista){

        try {
            cs.alugaBicicleta(idCiclista, idBicicleta);
        } catch (BicicletaIndisponivelException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }  catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(PATH_CICLISTA + "/devolveBicicleta")
    public void devolveBicicleta(@RequestParam int idBicicleta, @RequestParam int idCiclista, @RequestParam int idTranca){

        try {
            cs.devolveBicicleta(idCiclista, idBicicleta, idTranca);
        } catch (BicicletaIndisponivelException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }  catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
