package br.org.unirio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import br.org.unirio.exception.FuncionarioNotFoundException;
import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.Funcionario;
import br.org.unirio.service.FuncionarioService;

@RestController
public class FuncionarioController {

	@Autowired
	private FuncionarioService fs;

	private final String PATH_FUNCIONARIO = "/funcionario";

	@GetMapping(PATH_FUNCIONARIO)
	public List<Funcionario> getFuncionario(@RequestParam(required = false) Integer idFuncionario, @RequestParam(required = false) String nome, @RequestParam(required = false) String email) {
		if(idFuncionario == null)
			idFuncionario = 0;
		try {
			return fs.getFuncionario(idFuncionario, nome, email);
		} catch (FuncionarioNotFoundException fnfe) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping(PATH_FUNCIONARIO)
	public void criaFuncionario(@RequestBody Funcionario funcionario) {

		try {
			fs.criaFuncionario(funcionario);
		} catch (ObjetoJaExisteException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping(PATH_FUNCIONARIO)
	public void deletaFuncionario(@RequestParam Integer idFuncionario) {

		try {
			fs.deletaFuncionario(idFuncionario);
		} catch (FuncionarioNotFoundException fnfe) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
}
