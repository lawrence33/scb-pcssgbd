package br.org.unirio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.exception.PedidoAposentaTotemNotFoundException;
import br.org.unirio.exception.PedidoConsertoTotemNotFoundException;
import br.org.unirio.exception.TotemNotFoundException;
import br.org.unirio.model.PedidoAposentaTotem;
import br.org.unirio.model.PedidoConsertoTotem;
import br.org.unirio.model.Totem;
import br.org.unirio.service.TotemService;

@RestController
public class TotemController {

	@Autowired
	private TotemService ts;

	private final String PATH_TOTEM = "/totem";
	
	@PostMapping(PATH_TOTEM + "/conserta")
	public void consertaTotem(@RequestParam int idTotem, @RequestParam int idFuncionario) {
		try {
			ts.criaPedidoConsertaTotem(idTotem, idFuncionario);
		} catch (ObjetoJaExisteException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(PATH_TOTEM + "/conserta")
	public List<PedidoConsertoTotem> getConsertosTotem() {
		
		try {
			List<PedidoConsertoTotem> pedidos = ts.getPedidosDeConserto();
			return pedidos;
		} catch (PedidoConsertoTotemNotFoundException pctnfe) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		
	}

	@PostMapping(PATH_TOTEM + "/aposenta")
	public void aposentaTotem(@RequestParam int idTotem, @RequestParam int idFuncionario) {

		try {
			ts.criaPedidoAposentaTotem(idTotem, idFuncionario);
		} catch (ObjetoJaExisteException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(PATH_TOTEM + "/aposenta")
	public List<PedidoAposentaTotem> getAposentados() {
		try {
			List<PedidoAposentaTotem> pedidos = ts.getPedidosDeAposentadoria();
			return pedidos;
		} catch (PedidoAposentaTotemNotFoundException patnfe) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping(PATH_TOTEM)
	public Totem getTotem(@RequestParam Integer idTotem) {
		if (idTotem == null)
			idTotem = 0;
		try {
			return ts.getTotem(idTotem);

		} catch (TotemNotFoundException tnfe) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping(PATH_TOTEM)
	public void criaTotem(@RequestBody Totem totem) {

		try {
			ts.criaTotem(totem);
		} catch (ObjetoJaExisteException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

	}

	@DeleteMapping(PATH_TOTEM)
	public void deletaTotem(@RequestParam Integer idTotem) {

		try {
			ts.deletaTotem(idTotem);
		} catch (TotemNotFoundException fnfe) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
}
