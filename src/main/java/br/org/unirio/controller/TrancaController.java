package br.org.unirio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.exception.PedidoAposentaTrancaNotFoundException;
import br.org.unirio.exception.PedidoConsertoTrancaNotFoundException;
import br.org.unirio.exception.TrancaNotFoundException;
import br.org.unirio.model.PedidoAposentaTranca;
import br.org.unirio.model.PedidoConsertoTranca;
import br.org.unirio.model.Tranca;
import br.org.unirio.service.TrancaService;

@RestController
public class TrancaController {

	@Autowired
	private TrancaService ts;

	private final String PATH_TRANCA = "/tranca";

	@PostMapping(PATH_TRANCA + "/conserta")
	public void consertaTranca(@RequestParam int idTranca, @RequestParam int idFuncionario) {

		try {
			ts.criaPedidoConsertaTranca(idTranca, idFuncionario);
		} catch (ObjetoJaExisteException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(PATH_TRANCA + "/conserta")
	public List<PedidoConsertoTranca> getConsertosTranca() {

		try {
			List<PedidoConsertoTranca> pedidos = ts.getPedidosDeConserto();
			return pedidos;
		} catch (PedidoConsertoTrancaNotFoundException pctnfe) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping(PATH_TRANCA + "/aposenta")
	public void aposentaTranca(@RequestParam int idTranca, @RequestParam int idFuncionario) {

		try {
			ts.criaPedidoAposentaTranca(idTranca, idFuncionario);
		} catch (ObjetoJaExisteException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(PATH_TRANCA + "/aposenta")
	public List<PedidoAposentaTranca> getAposentadas() {
		try {
			List<PedidoAposentaTranca> pedidos = ts.getPedidosDeAposentadoria();
			return pedidos;
		} catch (PedidoAposentaTrancaNotFoundException patnfe) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping(PATH_TRANCA)
	public void criaTranca(@RequestBody Tranca tranca) {

		try {
			ts.criaTranca(tranca);
		} catch (ObjetoJaExisteException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(PATH_TRANCA)
	public Tranca getTranca(@RequestParam Integer idTranca) {
		if (idTranca == null)
			idTranca = 0;
		System.out.println(idTranca);
		try {
			return ts.getTranca(idTranca);
		} catch (TrancaNotFoundException tnfe) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

	}

	@DeleteMapping(PATH_TRANCA)
	public void deletaTranca(@RequestParam Integer idTranca) {

		try {
			ts.deletaTranca(idTranca);
		} catch (TrancaNotFoundException bnfe) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

}