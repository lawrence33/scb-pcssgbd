package br.org.unirio.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Aluguel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idAluguel;

	private int idCiclista;
	private int idBicicleta;
	
	private Date horaRetirada;
	private Date horaDevolucao;
	private double valor;

	@SuppressWarnings("deprecation")
	public void calculaValor() {
		BigDecimal duracaoAluguel = new BigDecimal(
				this.getHoraRetirada().getTime() - this.getHoraDevolucao().getTime());

		// R$ 15,00 por hora ou seja, 15 reais a cada 3600 segundos (3600000 ms)
		BigDecimal valorHora = new BigDecimal(15 / (3600 * 1000));

		BigDecimal valorTotal = duracaoAluguel.multiply(valorHora);
		this.valor = valorTotal.setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue();
	}
}
