package br.org.unirio.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "aposenta_bicleta")
public class PedidoAposentaBicicleta {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private int idBicicleta;

	private int idFuncionario;

}