package br.org.unirio.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "conserta_bicleta")
public class PedidoConsertoBicicleta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private int idFuncionario;
	
	private int idBicicleta;

	public PedidoConsertoBicicleta(int idBicicleta, int idFuncionario) {
		this.idFuncionario = idFuncionario;
		this.idBicicleta = idBicicleta;
	}

}