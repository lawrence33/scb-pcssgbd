package br.org.unirio.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "conserta_totem")
public class PedidoConsertoTotem {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private int idTotem;
	private int idFuncionario;

	public PedidoConsertoTotem(int idTotem, int idFuncionario) {
		this.idTotem = idTotem;
		this.idFuncionario = idFuncionario;
	}

}