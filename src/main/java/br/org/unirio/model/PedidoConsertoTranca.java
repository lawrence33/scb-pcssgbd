package br.org.unirio.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "conserta_tranca")
public class PedidoConsertoTranca {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
	
	@ManyToOne
	@JoinColumn(name = "id_funcionario")
    private Funcionario funcionarioRequisisante;
	
	@ManyToOne
	@JoinColumn(name = "id_tranca")
    private Tranca trancaDefeituosa;

    public PedidoConsertoTranca(Tranca tranca, Funcionario funcionario) {
        this.trancaDefeituosa = tranca;
        this.funcionarioRequisisante = funcionario;
    }

}