package br.org.unirio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.org.unirio.model.Aluguel;

@Repository
public interface AluguelRepository extends JpaRepository<Aluguel, Integer> {
	
	@Query("select a from Aluguel a where a.idBicicleta = ?1 and a.idCiclista = ?2 and a.horaDevolucao is null")
	Aluguel getAluguelEmAndamento(int idBicicleta, int idCiclista);

}
