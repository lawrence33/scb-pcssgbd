package br.org.unirio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.org.unirio.model.Bicicleta;

@Repository
public interface BicicletaRepository extends JpaRepository<Bicicleta, Integer> {
	Bicicleta findById(int id);
	
}