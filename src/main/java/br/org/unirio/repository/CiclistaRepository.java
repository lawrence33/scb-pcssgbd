package br.org.unirio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.org.unirio.model.Ciclista;
import org.springframework.stereotype.Repository;

@Repository
public interface CiclistaRepository extends JpaRepository<Ciclista, Integer> {
	Ciclista findById(int id);
	
	List<Ciclista> findByNomeAndEmail(String nome, String email);

	List<Ciclista> findByEmail(String email);

	List<Ciclista> findByNome(String nome);
}