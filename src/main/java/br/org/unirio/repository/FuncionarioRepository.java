package br.org.unirio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.org.unirio.model.Funcionario;

@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, Integer> {
	Funcionario findById(int id);

	List<Funcionario> findByNomeAndEmail(String nome, String email);


	List<Funcionario> findByNome(String nome);

	List<Funcionario> findByEmail(String email);
}