package br.org.unirio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.org.unirio.model.PedidoAposentaBicicleta;

@Repository
public interface PedidoAposentaBicicletaRepository extends JpaRepository<PedidoAposentaBicicleta, Integer> {

}