package br.org.unirio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.org.unirio.model.PedidoAposentaTotem;

@Repository
public interface PedidoAposentaTotemRepository extends JpaRepository<PedidoAposentaTotem, Integer> {
	PedidoAposentaTotem findById(int id);

	PedidoAposentaTotem findByIdFuncionarioAndIdTotem(int idFuncionario, int idTotem);
}