package br.org.unirio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.org.unirio.model.PedidoAposentaTranca;

@Repository
public interface PedidoAposentaTrancaRepository extends JpaRepository<PedidoAposentaTranca, Integer> {
	PedidoAposentaTranca findById(int id);
	
}