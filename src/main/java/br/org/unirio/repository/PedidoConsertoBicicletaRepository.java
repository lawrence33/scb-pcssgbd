package br.org.unirio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.org.unirio.model.PedidoConsertoBicicleta;

@Repository
public interface PedidoConsertoBicicletaRepository extends JpaRepository<PedidoConsertoBicicleta, Integer> {

}