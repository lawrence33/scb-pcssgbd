package br.org.unirio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.org.unirio.model.PedidoConsertoTotem;

@Repository
public interface PedidoConsertoTotemRepository extends JpaRepository<PedidoConsertoTotem, Integer> {
	PedidoConsertoTotem findById(int id);

	PedidoConsertoTotem findByIdFuncionarioAndIdTotem(int idFuncionario, int idTotem);
}