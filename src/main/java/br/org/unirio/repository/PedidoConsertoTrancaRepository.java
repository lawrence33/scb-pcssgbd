package br.org.unirio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.org.unirio.model.PedidoConsertoTranca;

@Repository
public interface PedidoConsertoTrancaRepository extends JpaRepository<PedidoConsertoTranca, Integer> {

}