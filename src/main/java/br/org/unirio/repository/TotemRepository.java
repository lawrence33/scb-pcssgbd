package br.org.unirio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.org.unirio.model.Totem;

@Repository
public interface TotemRepository extends JpaRepository<Totem, Integer> {
	Totem findById(int id);
}