package br.org.unirio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.org.unirio.model.Tranca;

@Repository
public interface TrancaRepository extends JpaRepository<Tranca, Integer> {
	Tranca findById(int id);
	
}