package br.org.unirio.service;

import java.util.List;

import br.org.unirio.model.*;
import br.org.unirio.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.org.unirio.exception.BicicletaNotFoundException;
import br.org.unirio.exception.FuncionarioNotFoundException;
import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.util.Status;

@Service
public class BicicletaService {

	@Autowired
	private BicicletaRepository bikeRepo;
	@Autowired
	private FuncionarioRepository funcRepo;
	@Autowired
	private PedidoConsertoBicicletaRepository pcbRepo;
	@Autowired
	private PedidoAposentaBicicletaRepository pabRepo;
	@Autowired
	private TrancaRepository trancaRepo;
	
	@Transactional(rollbackFor = Exception.class)
	public void criaPedidoConsertoBicicleta(int idBicicleta, int idFuncionario)
			throws ObjetoJaExisteException, FuncionarioNotFoundException {
		Bicicleta bicicleta = bikeRepo.findById(idBicicleta);
		Funcionario funcionario = funcRepo.findById(idFuncionario);

		PedidoConsertoBicicleta pcb = new PedidoConsertoBicicleta(idBicicleta, idFuncionario);

		if (funcionario == null || bicicleta == null)
			throw new FuncionarioNotFoundException();

		if (bicicleta.getStatus().equalsIgnoreCase(Status.EM_REPARO.getChave()))
			throw new ObjetoJaExisteException();
		bicicleta.setStatus(Status.EM_REPARO.getChave());
		bikeRepo.save(bicicleta);
		pcbRepo.save(pcb);
	}
	
	public List<PedidoConsertoBicicleta> getPedidosDeConserto() {
		return pcbRepo.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void criaPedidoAposentaBicicleta(int idBicicleta, int idFuncionario)
			throws ObjetoJaExisteException, FuncionarioNotFoundException {
		Bicicleta bicicleta = bikeRepo.findById(idBicicleta);
		Funcionario funcionario = funcRepo.findById(idFuncionario);

		if (funcionario == null || bicicleta == null)
			throw new FuncionarioNotFoundException();

		if (bicicleta.getStatus().equalsIgnoreCase(Status.APOSENTADA.getChave()))
			throw new ObjetoJaExisteException();
		bicicleta.setStatus(Status.APOSENTADA.getChave());
		bikeRepo.save(bicicleta);


		PedidoAposentaBicicleta pab = new PedidoAposentaBicicleta();
		pab.setIdBicicleta(idBicicleta);
		pab.setIdFuncionario(idFuncionario);
		pabRepo.save(pab);
	}

	public List<PedidoAposentaBicicleta> getPedidosDeAposentadoria() {
		return pabRepo.findAll();
	}
	
	@Transactional
	public void criaBicicleta(Bicicleta bicicletaACriar) throws Exception {
		Bicicleta bicicleta = bikeRepo.findById(bicicletaACriar.getId());

		if (bicicleta != null)
			throw new ObjetoJaExisteException();

		if(bicicletaACriar.getIdTranca() > 0){
			Tranca tranca =  trancaRepo.findById(bicicletaACriar.getIdTranca());
			if(!tranca.getStatus().equals(Status.DISPONIVEL.getChave()))
				throw new Exception();
			tranca.setStatus(Status.EM_USO.getChave());
			trancaRepo.save(tranca);
		}

		bikeRepo.save(bicicletaACriar);
	}

	public Bicicleta getBicicleta(int idBicicleta) throws BicicletaNotFoundException {
		Bicicleta bicicleta = bikeRepo.findById(idBicicleta);

		if (bicicleta == null)
			throw new BicicletaNotFoundException();

		return bicicleta;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletaBicicleta(int id) throws BicicletaNotFoundException {
		Bicicleta bicicleta = bikeRepo.findById(id);

		if (bicicleta == null)
			throw new BicicletaNotFoundException();

		bikeRepo.delete(bicicleta);
	}
}
