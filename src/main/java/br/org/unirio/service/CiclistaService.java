package br.org.unirio.service;

import br.org.unirio.exception.*;
import br.org.unirio.model.Aluguel;
import br.org.unirio.model.Bicicleta;
import br.org.unirio.model.Ciclista;
import br.org.unirio.model.Tranca;
import br.org.unirio.repository.AluguelRepository;
import br.org.unirio.repository.BicicletaRepository;
import br.org.unirio.repository.CiclistaRepository;
import br.org.unirio.repository.TrancaRepository;
import br.org.unirio.util.Status;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CiclistaService {
	@Autowired
	private CiclistaRepository ciclistaRepo;

	@Autowired
	private BicicletaRepository bikeRepo;

	@Autowired
	private AluguelRepository aluguelRepo;

	@Autowired
	private TrancaRepository trancaRepo;

	public List<Ciclista> getCiclista(int id, String nome, String email) throws CiclistaNotFoundException {
		List<Ciclista> ciclistas = new ArrayList<>();

		if (id > 0) {
			Ciclista ciclista = ciclistaRepo.findById(id);
			if (ciclista != null && (StringUtils.isEmpty(nome) || nome.equals(ciclista.getNome()))
					&& (StringUtils.isEmpty(email) || email.equals(ciclista.getEmail())))
				ciclistas.add(ciclista);
		}  else if(StringUtils.isEmpty(nome) && StringUtils.isEmpty(email)){
			ciclistas.addAll(ciclistaRepo.findAll());
		} else if(StringUtils.isEmpty(nome)){
			ciclistas.addAll(ciclistaRepo.findByEmail(email));
		} else if(StringUtils.isEmpty(email)) {
			ciclistas.addAll(ciclistaRepo.findByNome(nome));
		} else{
			ciclistas.addAll(ciclistaRepo.findByNomeAndEmail(nome, email));
		}

		if (ciclistas.size() == 0)
			throw new CiclistaNotFoundException();

		return ciclistas;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void criaCiclista(Ciclista ciclistaACriar) throws ObjetoJaExisteException {

		List<Ciclista> ciclistas = ciclistaRepo.findByEmail(ciclistaACriar.getEmail());

		if (ciclistas != null && ciclistas.size() > 0)
			throw new ObjetoJaExisteException();

		ciclistaRepo.save(ciclistaACriar);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletaCiclista(int id) throws CiclistaNotFoundException {
		Ciclista ciclista = ciclistaRepo.findById(id);

		if (ciclista == null)
			throw new CiclistaNotFoundException();

		ciclistaRepo.delete(ciclista);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void alugaBicicleta(int idCiclista, int idBicicleta)
			throws BicicletaIndisponivelException, BicicletaNotFoundException, CiclistaNotFoundException {
		Bicicleta bicicleta = bikeRepo.findById(idBicicleta);
		Ciclista ciclista = ciclistaRepo.findById(idCiclista);

		if (bicicleta == null)
			throw new BicicletaNotFoundException();

		if (ciclista == null)
			throw new CiclistaNotFoundException();

		if (!Status.DISPONIVEL.getChave().equals(bicicleta.getStatus()))
			throw new BicicletaIndisponivelException();

		Aluguel aluguel = new Aluguel();
		aluguel.setIdBicicleta(idBicicleta);
		aluguel.setIdCiclista(idCiclista);
		aluguel.setHoraRetirada(new Date());

		aluguelRepo.save(aluguel);

		bicicleta.setStatus(Status.EM_USO.getChave());
		bikeRepo.save(bicicleta);

		Tranca tranca = trancaRepo.findById(bicicleta.getIdTranca());
		tranca.setStatus(Status.DISPONIVEL.getChave());
		trancaRepo.save(tranca);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void devolveBicicleta(int idCiclista, int idBicicleta, int idTranca)
			throws BicicletaIndisponivelException, BicicletaNotFoundException, CiclistaNotFoundException,
			AluguelNotFoundException, TrancaIndisponivelException {
		Bicicleta bicicleta = bikeRepo.findById(idBicicleta);
		Ciclista ciclista = ciclistaRepo.findById(idCiclista);
		Tranca tranca = trancaRepo.findById(idTranca);

		if (bicicleta == null)
			throw new BicicletaNotFoundException();

		if (ciclista == null)
			throw new CiclistaNotFoundException();

		if (!Status.EM_USO.getChave().equals(bicicleta.getStatus()))
			throw new BicicletaIndisponivelException();

		if (!Status.DISPONIVEL.getChave().equals(tranca.getStatus()))
			throw new TrancaIndisponivelException();

		Aluguel aluguel = aluguelRepo.getAluguelEmAndamento(idBicicleta, idCiclista);

		if (aluguel == null)
			throw new AluguelNotFoundException();

		aluguel.setHoraDevolucao(new Date());
		aluguel.calculaValor();

		aluguelRepo.save(aluguel);

		bicicleta.setStatus(Status.DISPONIVEL.getChave());
		bikeRepo.save(bicicleta);


		tranca.setStatus(Status.EM_USO.getChave());
		trancaRepo.save(tranca);
	}
}
