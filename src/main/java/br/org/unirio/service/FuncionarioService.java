package br.org.unirio.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.org.unirio.exception.FuncionarioNotFoundException;
import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.Funcionario;
import br.org.unirio.repository.FuncionarioRepository;

@Service
public class FuncionarioService {
	
	@Autowired
	private FuncionarioRepository funcRepo;

	public List<Funcionario> getFuncionario(int id, String nome, String email) throws FuncionarioNotFoundException {
		List<Funcionario> funcionarios = new ArrayList<>();
		if (id != 0) {
			Funcionario funcionario = funcRepo.findById(id);
			if (funcionario != null && (StringUtils.isEmpty(nome) || nome.equalsIgnoreCase(funcionario.getNome()))
					&& (StringUtils.isEmpty(email) || email.equals(funcionario.getEmail())))
				funcionarios.add(funcionario);
		} else if(StringUtils.isEmpty(nome) && StringUtils.isEmpty(email)){
			funcionarios.addAll(funcRepo.findAll());
		} else if(StringUtils.isEmpty(nome)){
			funcionarios.addAll(funcRepo.findByEmail(email));
		} else if(StringUtils.isEmpty(email)) {
			funcionarios.addAll(funcRepo.findByNome(nome));
		} else{
			funcionarios.addAll(funcRepo.findByNomeAndEmail(nome, email));
		}

		if (funcionarios.size() == 0)
			throw new FuncionarioNotFoundException();

		return funcionarios;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void criaFuncionario(Funcionario funcionarioACriar) throws ObjetoJaExisteException {
		List<Funcionario> funcionarios = funcRepo.findByNomeAndEmail(funcionarioACriar.getNome(), funcionarioACriar.getEmail());

		if (funcionarios != null && funcionarios.size() > 0)
			throw new ObjetoJaExisteException();

		funcRepo.save(funcionarioACriar);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletaFuncionario(int id) throws FuncionarioNotFoundException {
		Funcionario funcionario = funcRepo.findById(id);

		if (funcionario == null)
			throw new FuncionarioNotFoundException();

		funcRepo.delete(funcionario);
	}
}
