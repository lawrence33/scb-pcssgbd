package br.org.unirio.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.exception.PedidoAposentaTotemNotFoundException;
import br.org.unirio.exception.PedidoConsertoTotemNotFoundException;
import br.org.unirio.exception.TotemNotFoundException;
import br.org.unirio.model.Funcionario;
import br.org.unirio.model.PedidoAposentaTotem;
import br.org.unirio.model.PedidoConsertoTotem;
import br.org.unirio.model.Totem;
import br.org.unirio.repository.FuncionarioRepository;
import br.org.unirio.repository.PedidoAposentaTotemRepository;
import br.org.unirio.repository.PedidoConsertoTotemRepository;
import br.org.unirio.repository.TotemRepository;
import br.org.unirio.util.Status;

@Service
public class TotemService {

	@Autowired
	private TotemRepository totemRepo;
	@Autowired
	private FuncionarioRepository funcRepo;
	@Autowired
	private PedidoConsertoTotemRepository pctRepo;
	@Autowired
	private PedidoAposentaTotemRepository patRepo;

	@Transactional(rollbackFor = Exception.class)
	public void criaPedidoConsertaTotem(int idTotem, int idFuncionario) throws Exception {
		Totem totem = totemRepo.findById(idTotem);
		Funcionario funcionario = funcRepo.findById(idFuncionario);

		PedidoConsertoTotem pedido = new PedidoConsertoTotem(idTotem, idFuncionario);

		if (totem == null || funcionario == null) {
			throw new Exception();
		}

		if (pctRepo.findByIdFuncionarioAndIdTotem(idFuncionario, idTotem) != null)
			throw new ObjetoJaExisteException();
		pctRepo.save(pedido);
		totem.setStatus(Status.EM_REPARO.getChave());
		totemRepo.save(totem);
	}

	@Transactional(rollbackFor = Exception.class)
	public List<PedidoConsertoTotem> getPedidosDeConserto() throws PedidoConsertoTotemNotFoundException {
		
		List<PedidoConsertoTotem> consertos = pctRepo.findAll();

		if (consertos == null || consertos.size() < 1) {
			throw new PedidoConsertoTotemNotFoundException();
		}
		return consertos;
	}

	@Transactional(rollbackFor = Exception.class)
	public void criaPedidoAposentaTotem(int idTotem, int idFuncionario) throws Exception {
		Totem totem = totemRepo.findById(idTotem);
		Funcionario funcionario = funcRepo.findById(idFuncionario);

		PedidoAposentaTotem pedido = new PedidoAposentaTotem(idTotem, idFuncionario);

		if (totem == null || funcionario == null) {
			throw new Exception();
		}

		if (patRepo.findByIdFuncionarioAndIdTotem(idFuncionario, idTotem) != null)
			throw new ObjetoJaExisteException();
		patRepo.save(pedido);
		totem.setStatus(Status.APOSENTADA.getChave());
		totemRepo.save(totem);
	}

	public List<PedidoAposentaTotem> getPedidosDeAposentadoria() throws PedidoAposentaTotemNotFoundException {

		List<PedidoAposentaTotem> aposentadorias = patRepo.findAll();

		if (aposentadorias == null || aposentadorias.size() < 1) {
			throw new PedidoAposentaTotemNotFoundException();
		}
		return aposentadorias;
	}

	@Transactional(rollbackFor = Exception.class)
	public void criaTotem(Totem totemACriar) throws ObjetoJaExisteException {
		Totem totem = totemRepo.findById(totemACriar.getId());

		if (totem != null)
			throw new ObjetoJaExisteException();

		totemRepo.save(totemACriar);
	}

	public Totem getTotem(int idTotem) throws TotemNotFoundException {
		Totem totem = totemRepo.findById(idTotem);

		if (totem == null) {
			throw new TotemNotFoundException();
		}

		return totem;
	}

	@Transactional(rollbackFor = Exception.class)
	public void deletaTotem(int id) throws TotemNotFoundException {
		Totem totem = totemRepo.findById(id);

		if (totem == null)
			throw new TotemNotFoundException();

		totemRepo.delete(totem);
	}
}
