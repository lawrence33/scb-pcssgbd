package br.org.unirio.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.org.unirio.exception.FuncionarioNotFoundException;
import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.exception.PedidoAposentaTrancaNotFoundException;
import br.org.unirio.exception.PedidoConsertoTrancaNotFoundException;
import br.org.unirio.exception.TrancaNotFoundException;
import br.org.unirio.model.Funcionario;
import br.org.unirio.model.PedidoAposentaTranca;
import br.org.unirio.model.PedidoConsertoTranca;
import br.org.unirio.model.Tranca;
import br.org.unirio.repository.FuncionarioRepository;
import br.org.unirio.repository.PedidoAposentaTrancaRepository;
import br.org.unirio.repository.PedidoConsertoTrancaRepository;
import br.org.unirio.repository.TrancaRepository;
import br.org.unirio.util.Status;

@Service
public class TrancaService {

	@Autowired
	private FuncionarioRepository funcRepo;
	@Autowired
	private TrancaRepository trancaRepo;
	@Autowired
	private PedidoConsertoTrancaRepository pctRepo;
	@Autowired
	private PedidoAposentaTrancaRepository patRepo;

	@Transactional(rollbackFor = Exception.class)
	public void criaPedidoConsertaTranca(int idTranca, int idFuncionario)
			throws ObjetoJaExisteException, FuncionarioNotFoundException {
		Tranca tranca = trancaRepo.findById(idTranca);
		Funcionario funcionario = funcRepo.findById(idFuncionario);

		if (funcionario == null || tranca == null)
			throw new FuncionarioNotFoundException();

		if (tranca.getStatus().equalsIgnoreCase(Status.EM_REPARO.getChave()))
			throw new ObjetoJaExisteException();
		tranca.setStatus(Status.EM_REPARO.getChave());
		trancaRepo.save(tranca);
	}

	@Transactional(rollbackFor = Exception.class)
	public List<PedidoConsertoTranca> getPedidosDeConserto() throws PedidoConsertoTrancaNotFoundException {

		List<PedidoConsertoTranca> consertos = pctRepo.findAll();

		if (consertos == null || consertos.size() < 1) {
			throw new PedidoConsertoTrancaNotFoundException();
		}
		return consertos;
	}

	@Transactional(rollbackFor = Exception.class)
	public void criaPedidoAposentaTranca(int idTranca, int idFuncionario)
			throws ObjetoJaExisteException, FuncionarioNotFoundException {
		Tranca tranca = trancaRepo.findById(idTranca);
		Funcionario funcionario = funcRepo.findById(idFuncionario);

		if (funcionario == null || tranca == null)
			throw new FuncionarioNotFoundException();

		if (tranca.getStatus().equalsIgnoreCase(Status.APOSENTADA.getChave()))
			throw new ObjetoJaExisteException();
		tranca.setStatus(Status.APOSENTADA.getChave());
		trancaRepo.save(tranca);
	}

	public List<PedidoAposentaTranca> getPedidosDeAposentadoria() throws PedidoAposentaTrancaNotFoundException {

		List<PedidoAposentaTranca> aposentadorias = patRepo.findAll();

		if (aposentadorias == null || aposentadorias.size() < 1) {
			throw new PedidoAposentaTrancaNotFoundException();
		}
		return aposentadorias;
	}

	@Transactional(rollbackFor = Exception.class)
	public void criaTranca(Tranca trancaACriar) throws ObjetoJaExisteException {
		Tranca tranca = trancaRepo.findById(trancaACriar.getId());

		if (tranca != null)
			throw new ObjetoJaExisteException();

		trancaRepo.save(trancaACriar);
	}
	
	public Tranca getTranca(int idTranca) throws TrancaNotFoundException {
		Tranca tranca = trancaRepo.findById(idTranca);

		if (tranca == null)
			throw new TrancaNotFoundException();

		return tranca;
	}

	@Transactional(rollbackFor = Exception.class)
	public void deletaTranca(int id) throws TrancaNotFoundException {
		Tranca tranca = trancaRepo.findById(id);

		if (tranca == null)
			throw new TrancaNotFoundException();

		trancaRepo.delete(tranca);
	}
}
