package br.org.unirio.util;

public enum Status {
    EM_USO("EM_USO"),
    DISPONIVEL("DISPONIVEL"),
    REPARO_SOLICITADO("REPARO_SOLICITADO"),
    EM_REPARO("EM_REPARO"),
    APOSENTADA("APOSENTADA");

    private String chave;

    Status(String chave) {
        this.chave = chave;
    }

    public String getChave() {
        return chave;
    }
}
