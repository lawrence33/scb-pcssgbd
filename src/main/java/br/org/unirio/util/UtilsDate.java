package br.org.unirio.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilsDate {

    public static Date retornaDataBr(String data){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return formatter.parse(data);
        } catch (ParseException e) {
            return null;
        }
    }
}
